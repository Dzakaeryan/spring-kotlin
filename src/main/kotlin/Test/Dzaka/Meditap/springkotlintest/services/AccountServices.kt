package Test.Dzaka.Meditap.springkotlintest.services

import Test.Dzaka.Meditap.springkotlintest.error.NotFoundException
import Test.Dzaka.Meditap.springkotlintest.model.Account
import Test.Dzaka.Meditap.springkotlintest.repository.AccountRepository
import org.springframework.stereotype.Service
import Test.Dzaka.Meditap.springkotlintest.validator.RequestGetBalance
import Test.Dzaka.Meditap.springkotlintest.validator.RequestTransferBalance
import Test.Dzaka.Meditap.springkotlintest.validator.ResponseGetBalance
import Test.Dzaka.Meditap.springkotlintest.validator.ResponseTransferBalance
import org.springframework.data.repository.findByIdOrNull
import javax.transaction.Transactional

@Service
class AccountServices (
    val accountRepository: AccountRepository,
){
    @Transactional
    @Override fun transferBalance(params: RequestTransferBalance) : ResponseTransferBalance? {
        val sender = findAccountBalance(params.fromAccount)
        val receiver = findAccountBalance(params.toAccount)

        if(sender === null || receiver === null) {
            return ResponseTransferBalance(
                params.fromAccount,
                params.toAccount,
                0,
                params.transfer,
                "Nomor rekening yang Dimasukan salah ",)
        }

        if (sender.balance > params.transfer) {
            val totalreceiver = receiver.balance + params.transfer
            val totalsender = sender.balance - params.transfer

            receiver.apply {
                balance = totalreceiver
            }

            sender.apply {
                balance = totalsender
            }

            accountRepository.save(receiver)
            accountRepository.save(sender)

            return ResponseTransferBalance(
                params.fromAccount,
                params.toAccount,
                sender.balance,
                params.transfer,
                "succes",
            )
        } else {
            return ResponseTransferBalance(
                params.fromAccount,
                params.toAccount,
                sender.balance,
                params.transfer,
                "Saldo Kurang",
            )
        }
    }

    @Override fun getBalance(params: RequestGetBalance) : ResponseGetBalance? {
        try {
            val data = findAccountBalance(params.accountNumber)
            if (data === null) {
                return data
            }
            return ResponseGetBalance(
                data?.accountHolder,
                data?.balance,
            )
        } catch (e:IllegalAccessError) {
            throw NotFoundException()
        }
    }

    private fun findAccountBalance(id: Int): Account? {
        val balance = accountRepository.findByIdOrNull(id)
        if (balance == null) {
         return balance
        } else {
            return balance
        }
    }
}