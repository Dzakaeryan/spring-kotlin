package Test.Dzaka.Meditap.springkotlintest.validator

data class ResponseTransferBalance(
    val fromAccount: Int,
    val toAccount: Int,
    var balance: Long,
    var transfer: Long,
    val message: String
)
