package Test.Dzaka.Meditap.springkotlintest.validator

data class WebResponse<T>(

    val code: Int,

    val status: String,

    val data: T
)