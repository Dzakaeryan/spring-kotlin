package Test.Dzaka.Meditap.springkotlintest.validator

data class ResponseGetBalance(
       var  accountHolder: String,
       var  balance: Long,
)
