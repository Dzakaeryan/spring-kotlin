package Test.Dzaka.Meditap.springkotlintest.validator


data class RequestTransferBalance (
    val  fromAccount: Int,
    val  toAccount: Int,
    val  transfer: Long,
)
