package Test.Dzaka.Meditap.springkotlintest.validator

import org.jetbrains.annotations.NotNull

data class RequestGetBalance(
    @NotNull
    val  accountNumber: Int,
)
