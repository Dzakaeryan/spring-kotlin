package Test.Dzaka.Meditap.springkotlintest.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "account")
data class Account(
    @Id
    @Column(name = "accountnumber")
    val  accountNumber: Int,

    @Column(name = "accountholder")
    val  accountHolder: String,

    @Column(name = "balance")
    var balance: Long,
)
