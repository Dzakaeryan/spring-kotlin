package Test.Dzaka.Meditap.springkotlintest.repository

import Test.Dzaka.Meditap.springkotlintest.model.Account
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface  AccountRepository:JpaRepository<Account, Int> {
    // fun findByAgeLessThan(age: Int?): List<Account?>?
    // fun findByAccountNumber(accountNumber: Int): Account?
}