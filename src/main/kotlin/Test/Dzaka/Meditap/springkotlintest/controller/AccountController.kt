package Test.Dzaka.Meditap.springkotlintest.controller

import Test.Dzaka.Meditap.springkotlintest.validator.WebResponse
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import Test.Dzaka.Meditap.springkotlintest.services.AccountServices
import Test.Dzaka.Meditap.springkotlintest.validator.RequestGetBalance
import Test.Dzaka.Meditap.springkotlintest.validator.RequestTransferBalance
import Test.Dzaka.Meditap.springkotlintest.validator.ResponseGetBalance
import Test.Dzaka.Meditap.springkotlintest.validator.ResponseTransferBalance

@RestController
class AccountController(val accountServices: AccountServices) {
    @PostMapping(
        value = ["/api/transfer"],
        produces = ["application/json"],
        consumes = ["application/json"]
    )
    fun accountTransfer(@RequestBody body: RequestTransferBalance): WebResponse<ResponseTransferBalance?> {
            val data = accountServices.transferBalance(body)
            return WebResponse(
                code = 200,
                status = "OK",
                data = data
            )
    }

    @PostMapping(
        value = ["/api/balance"],
        produces = ["application/json"],
        consumes = ["application/json"])
    fun getBalance(@RequestBody body: RequestGetBalance): WebResponse<ResponseGetBalance?> {
        val data = accountServices.getBalance(body)
        return WebResponse(
            code = 200,
            status = "OK",
            data = data
        )
    }

}